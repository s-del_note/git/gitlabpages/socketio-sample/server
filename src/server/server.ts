import SocketIO = require('socket.io');
import https = require('https');
import * as requestEvent from './event/request';

const userMap: UserMap = new Map<string, User>();

const defineEvent = (
  io: SocketIO.Server,
  userMap: UserMap
): void => {
  io.on('connection', (socket: SocketIO.Socket) => {
    requestEvent.connectionEvent(socket, userMap);
    requestEvent.changeNameEvent(socket, userMap);
    requestEvent.sendMessageEvent(io, socket, userMap);
    requestEvent.disconnectEvent(socket, userMap);
  });
};

export const createServer = (key: Buffer, cert: Buffer): https.Server => {
  const server = https.createServer({ key, cert });
  const io = new SocketIO.Server(server, {
    cors: { origin: 'https://s-del_note.gitlab.io' },
    cookie: false,
    serveClient: false
  });
  defineEvent(io, userMap);

  return server;
};
