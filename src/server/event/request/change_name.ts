import SocketIO = require('socket.io');
import * as guard from '../../../common/guard';
import * as user from '../../user';
import * as responseEvent from '../../event/response';

const isValid = (newName: string): boolean => {
  return newName.length <= 10 && !guard.isEmptyString(newName);
};

export const changeNameEvent = (
  socket: SocketIO.Socket,
  userMap: UserMap
): void => {
  socket.on('change_name', (newName: unknown) => {
    if (!guard.isString(newName)) return;
    if (!isValid(newName)) return;

    const changeNameUser = userMap.get(socket.id);
    if (!changeNameUser) return;
    if (!user.canAction(changeNameUser)) return;

    const currentName = changeNameUser.name;
    changeNameUser.name = newName.trim().replace(/\s+/g, ' ');
    changeNameUser.lastActionTime = Date.now();
    console.log([
      `${socket.handshake.address}`,
      'change name:',
      `${currentName} -> ${changeNameUser.name}`
    ].join(' '));
    responseEvent.updateName(socket, changeNameUser);
  });
};
