import SocketIO = require('socket.io');
import * as guard from '../../../common/guard';
import * as user from '../../user';

const isValid = (newMessage: string): boolean => {
  return newMessage.length <= 140 && !guard.isEmptyString(newMessage);
}

export const sendMessageEvent = (
  io: SocketIO.Server,
  socket: SocketIO.Socket,
  userMap: UserMap
): void => {
  socket.on('send_message', (newMessage: unknown) => {
    if (!guard.isString(newMessage)) return;
    if (!isValid(newMessage)) return;

    const sendMessageUser = userMap.get(socket.id);
    if (!sendMessageUser) return;
    if (!user.canAction(sendMessageUser)) return;

    const validMessage = newMessage.trim().replace(/\s+/g, ' ');
    sendMessageUser.lastActionTime = Date.now();
    console.log([
      sendMessageUser.name,
      `(${sendMessageUser.id}):`,
      validMessage
    ].join(' '));
    io.emit('response_message',
            `${sendMessageUser.name} (${sendMessageUser.id})`,
            validMessage);
  });
};
