import SocketIO = require('socket.io');

export const updateName = (socket: SocketIO.Socket, user: User): void => {
  socket.emit('update_name', `${user.name} (${user.id})`);
};
