import crypto = require('crypto');

export const createUserID = (ip: string): string => crypto.createHash('sha256')
                                                          .update(ip, 'utf8')
                                                          .digest('base64');
