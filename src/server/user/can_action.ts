export const canAction = (user: User): boolean => {
  return Date.now() - user.lastActionTime > 1200;
}
