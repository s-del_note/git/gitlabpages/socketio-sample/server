export const isString = (arg: unknown): arg is string => {
  return typeof arg === 'string';
};

export const isEmptyString = (arg: string): boolean => {
  return !arg || arg.length <= 1 || /^\s+$/.test(arg);
};
